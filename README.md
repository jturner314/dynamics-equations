# Advanced Dynamics Equations

This is a summary of useful equations from advanced rigid body dynamics.

See the [tags](https://gitlab.com/jturner314/dynamics-equations/tags) page for
PDF downloads.

## License

Copyright (C) 2016  Jim Turner

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License. To view a copy of this license, visit
https://creativecommons.org/licenses/by-sa/4.0/.
