% Copyright (C) 2016  Jim Turner
%
% This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
% To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/.

\documentclass[10pt]{article}

\newcommand{\thetitle}{Advanced Dynamics}
\newcommand{\theauthor}{Jim Turner}

\usepackage{mystyle}

\begin{document}

\fancypagestyle{plain}{
\fancyfoot[C]{Copyright \textcopyright\ 2016 Jim Turner. This work is licensed under the Creative
  Commons Attribution-ShareAlike~4.0 International License. To view a copy of this license, visit
  \mbox{\url{https://creativecommons.org/licenses/by-sa/4.0/}}. The \LaTeX{} source files are
  available under the same license at \mbox{\url{https://gitlab.com/jturner314/dynamics-equations}}.}
}

\maketitle
\tableofcontents

\section*{Thanks}

The notation system and equations in this document are derived from course
lectures of \href{https://www.mae.ncsu.edu/people/apmazzol}{Dr.~Andre
  Mazzoleni} of North Carolina State University for his MAE 511 Advanced
Dynamics class in Fall 2016.

\section*{Contributing}

This document and its sources are available under a free license (see the
footer) in the hopes that you will find them useful and so that you have the
freedom to make them even better. All development for this equation sheet and
others is done on GitLab. If you'd like to contribute, please visit the project
page at the URL provided in the footer. If you find an error or other area for
improvement, please create an issue or send a pull request via GitLab.

\newpage

\section{Notation}

\begin{itemize}
\item Vectors are written as lowercase, bold, italic, serif letters, such as
  \(\vec{a}\), and unit vectors are written with a hat, such as \(\uvec{a}\).
\item Reference frame: \(\rframe{F} = \rframedef{F}\), where \(F\) is the
  origin of the reference frame and \(\ivec{F}\), \(\jvec{F}\), and
  \(\kvec{F}\) are unit vectors along the axes of the reference frame.
  \emph{Note that all reference frames in this document are assumed to have
    orthogonal unit vectors and a right-hand orientation.}
\item Derivative of \(\vec{q}\) with respect to frame \(\rframe{F}\):
  \(\rdif{F}\vec{q} = \dot{x}_\rframe{F} \ivec{F} + \dot{y}_\rframe{F} \jvec{F}
  + \dot{z}_\rframe{F} \kvec{F}\) where
  \(\vec{q} = x_\rframe{F} \ivec{F} + y_\rframe{F} \jvec{F} + z_\rframe{F} \kvec{F}\)
\item Coordinates of vector \(\vec{q}\) with respect to frame \(\rframe{F}\):
  \(\coord{\vec{q}}{F} = \begin{Bmatrix} \vec{q} \vecdot \ivec{F} \\ \vec{q} \vecdot \jvec{F} \\
    \vec{q} \vecdot \kvec{F} \end{Bmatrix} =\begin{Bmatrix} x_\rframe{F} \\ y_\rframe{F} \\
    z_\rframe{F} \end{Bmatrix}\)
\item Direction cosine matrix expressing rotation between \(\rframe{B}\) and \(\rframe{A}\):
  \[\rot{A}{B} =
  \begin{bmatrix}
    \ivec{A} \vecdot \ivec{B} & \ivec{A} \vecdot \jvec{B} & \ivec{A} \vecdot \kvec{B} \\
    \jvec{A} \vecdot \ivec{B} & \jvec{A} \vecdot \jvec{B} & \jvec{A} \vecdot \kvec{B} \\
    \kvec{A} \vecdot \ivec{B} & \kvec{A} \vecdot \jvec{B} & \kvec{A} \vecdot \kvec{B} \\
  \end{bmatrix}\]
\item Angular velocity of \(\rframe{B}\) with respect to \(\rframe{A}\): \(\angvel{A}{B}\)
\item Position of point \(D\) relative to point \(C\): \(\pos{D/C}\)
\item Position of point mass \(m\) relative to point \(C\): \(\pos{m/C}\)
\item Velocity of \(D\) relative to \(C\) with respect to \(\rframe{F}\): \(\vel{F}{D/C} = \rdif{F}\pos{D/C}\)
\item Acceleration of \(D\) relative to \(C\) with respect to \(\rframe{F}\): \(\accel{F}{D/C} = \rdif{F}\prn{\vel{F}{D/C}} = \rdif[2]{F}\pos{D/C}\)
\item Time: \(t\)
\item Tensors are written as uppercase, bold, italic, sans-serif letters, such as \(\tens{T}\).
\item The dyadic product of vectors \(\vec{a}\) and \(\vec{b}\) is written
  as \(\vec{a} \vec{b}\); two of its properties are
  \begin{align*}
    \prn{\vec{a}\vec{b}} \vecdot \vec{c} &\equiv \vec{a} \prn{\vec{b} \vecdot \vec{c}} \\
    \vec{c} \vecdot \prn{\vec{a}\vec{b}} &\equiv \prn{\vec{c} \vecdot \vec{a}} \vec{b}
  \end{align*}
  Note that the dyadic product of vectors \(\vec{a}\) and \(\vec{b}\) can be expressed as
  \begin{equation*}
    \vec{a}\vec{b} =
    \prn{
      \begin{aligned}
        &a_xb_x\ivec{F}\ivec{F} + a_xb_y\ivec{F}\jvec{F} + a_xb_z\ivec{F}\kvec{F} \\
        +\ &a_xb_x\jvec{F}\ivec{F} + a_yb_y\jvec{F}\jvec{F} + a_yb_z\jvec{F}\kvec{F} \\
        +\ &a_zb_x\kvec{F}\ivec{F} + a_zb_y\kvec{F}\jvec{F} + a_zb_z\kvec{F}\kvec{F} \\
      \end{aligned}
    }
  \end{equation*}
  where
  \begin{align*}
    \vec{a} &= a_x\ivec{F} + a_y\ivec{F} + a_z\ivec{F} \\
    \vec{b} &= b_x\ivec{F} + b_y\ivec{F} + b_z\ivec{F}
  \end{align*}
\end{itemize}

\section{Kinematics}

\subsection{Derivatives}

\begin{itemize}
\item Transport theorem: for any vector \(\vec{q}\) and any frames \(\rframe{A}\) and \(\rframe{B}\),
  \begin{equation*}
    \rdif{A}{\vec{q}} = \rdif{B}{\vec{q}} + \angvel{A}{B} \vectimes \vec{q}
  \end{equation*}
\item Transport theorem for second derivatives: for any vector \(\vec{q}\) and any frames \(\rframe{A}\) and \(\rframe{B}\),
  \begin{equation*}
    \rdif[2]{A}{\vec{q}}
    = \rdif[2]{B}{\vec{q}} + 2 \angvel{A}{B} \vectimes \rdif{B}{\vec{q}} + \angaccel{A}{B} \vectimes \vec{q}
    + \angvel{A}{B} \vectimes \prn{\angvel{A}{B} \vectimes \vec{q}}
  \end{equation*}
\item Scalar--vector product rule: given any vector \(\vec{q}\), any scalar \(\beta\), and any frame \(\rframe{F}\),
  \begin{equation*}
    \rdif{F}\prn{\beta \vec{q}} = \dot{\beta} \vec{q} + \beta \rdif{F}{\vec{q}}
  \end{equation*}
\item Vector--vector product rule: given any vectors \(\vec{q}\) and \(\vec{r}\), and any frame \(\rframe{F}\),
  \begin{equation*}
    \rdif{F}\prn{\vec{q} \vectimes \vec{r}} = \prn{\rdif{F}\vec{q}} \vectimes \vec{r} + \vec{q} \vectimes \prn{\rdif{F}{\vec{r}}}
  \end{equation*}
\item For any vector \(\vec{q}\) and any frame \(\rframe{F}\),
  \begin{equation*}
    \coord{\rdif{F}{\vec{q}}}{F} = \od{}{t} \prn{\coord{\vec{q}}{F}}
    = \begin{Bmatrix}
      \dot{q}_x \\
      \dot{q}_y \\
      \dot{q}_z
    \end{Bmatrix}
  \end{equation*}
  where
  \begin{equation*}
    \vec{q} = q_x \ivec{F} + q_y \jvec{F} + q_z \kvec{F}
  \end{equation*}
\end{itemize}

\subsection{Rotation}

\begin{itemize}
\item For any vector \(\vec{q}\), any frame \(\rframe{A}\), and any frame \(\rframe{B}\),
  \begin{align*}
    \coord{\vec{q}}{A} &= \rot{A}{B} \vecdot \coord{\vec{q}}{B} \\
    \coord{\vec{q}}{B} &= \rot{B}{A} \vecdot \coord{\vec{q}}{A}
  \end{align*}
\item Inverse of rotation matrix is transpose
  \begin{equation*}
    \rot{B}{A} = \inverse{\prn{\rot{A}{B}}} = \transpose{\prn{\rot{A}{B}}}
  \end{equation*}
\item Composition of rotations
  \begin{equation*}
    \rot{A}{C} = \rot{A}{B} \vecdot \rot{B}{C}
  \end{equation*}
\item Single axis rotation matrices
  \begin{itemize}
  \item Rotation from \(\rframe{A}\) to \(\rframe{B}\) about \(\ivec{A} = \ivec{B}\) through angle \(\theta\):
    \begin{equation*}
      \rot{B}{A} = {\sbr{R(\theta)}}_x
      \equiv \begin{bmatrix}
        1 & 0 & 0 \\
        0 & \cos\theta & \sin\theta \\
        0 & -\sin\theta & \cos\theta \\
      \end{bmatrix}
    \end{equation*}
  \item Rotation from \(\rframe{A}\) to \(\rframe{B}\) about \(\jvec{A} = \jvec{B}\) through angle \(\theta\):
    \begin{equation*}
      \rot{B}{A} = {\sbr{R(\theta)}}_y
      \equiv \begin{bmatrix}
        \cos\theta & 0 & -\sin\theta \\
        0 & 1 & 0 \\
        \sin\theta & 0 & \cos\theta \\
      \end{bmatrix}
    \end{equation*}
  \item Rotation from \(\rframe{A}\) to \(\rframe{B}\) about \(\kvec{A} = \kvec{B}\) through angle \(\theta\):
    \begin{equation*}
      \rot{B}{A} = {\sbr{R(\theta)}}_z
      \equiv \begin{bmatrix}
        \cos\theta & \sin\theta & 0 \\
        -\sin\theta & \cos\theta & 0 \\
        0 & 0 & 1 \\
      \end{bmatrix}
    \end{equation*}
  \end{itemize}
\end{itemize}

\subsection{Angular Velocity}

\begin{itemize}
\item Angular velocity
  \begin{equation*}
    \angvel{A}{B} = \ivec{B} \sbr{\prn{\rdif{A}{\jvec{B}}} \vecdot \kvec{B}} + \jvec{B} \sbr{\prn{\rdif{A}{\kvec{B}}} \vecdot \ivec{B}} + \kvec{B} \sbr{\prn{\rdif{A}{\ivec{B}}} \vecdot \jvec{B}}
  \end{equation*}
\item Angular velocity for rotation of \(\rframe{B}\) with respect to \(\rframe{A}\) about \(\kvec{A} = \kvec{B}\) through angle \(\theta\)
  \begin{equation*}
    \angvel{A}{B} = \dot{\theta} \kvec{A} = \dot{\theta} \kvec{B}
  \end{equation*}
\item Addition of angular velocities
  \begin{equation*}
    \angvel{A}{C} = \angvel{A}{B} + \angvel{B}{C}
  \end{equation*}
\item For any vector \(\vec{q}\) and any frames \(\rframe{A}\) and \(\rframe{B}\),
  \begin{align*}
    \coord{\angvel{A}{B} \vectimes \vec{q}}{B}
    &= \rot{B}{A} \vecdot \dot{\rot{A}{B}} \vecdot \coord{\vec{q}}{B} \\
    \omega_{x\rframe{B}}
    &= \begin{Bmatrix} 0 & 0 & 1 \end{Bmatrix} \vecdot \rot{B}{A} \vecdot \dot{\rot{A}{B}} \vecdot \begin{Bmatrix} 0 \\ 1 \\ 0 \end{Bmatrix} \\
    \omega_{y\rframe{B}}
    &= \begin{Bmatrix} 1 & 0 & 0 \end{Bmatrix} \vecdot \rot{B}{A} \vecdot \dot{\rot{A}{B}} \vecdot \begin{Bmatrix} 0 \\ 0 \\ 1 \end{Bmatrix} \\
    \omega_{z\rframe{B}}
    &= \begin{Bmatrix} 0 & 1 & 0 \end{Bmatrix} \vecdot \rot{B}{A} \vecdot \dot{\rot{A}{B}} \vecdot \begin{Bmatrix} 1 \\ 0 \\ 0 \end{Bmatrix}
  \end{align*}
  where \(\angvel{A}{B} = \omega_{x\rframe{B}} \ivec{B} + \omega_{y\rframe{B}} \jvec{B} + \omega_{z\rframe{B}} \kvec{B}\)
\end{itemize}

\subsection{Angular Acceleration}

\begin{itemize}
\item Angular acceleration
  \begin{equation*}
    \angaccel{A}{B} = \rdif{A}{\angvel{A}{B}} = \rdif{B}{\angvel{A}{B}}
  \end{equation*}
\item Composition of angular accelerations
  \begin{equation*}
    \angaccel{A}{C} = \angaccel{A}{B} + \angaccel{B}{C} + \angvel{A}{B} \vectimes \angvel{B}{C}
  \end{equation*}
\end{itemize}

\subsection{Coordinates}

\begin{itemize}
\item For any vectors \(\vec{q}_1\) and \(\vec{q}_2\) and any frame \(\rframe{F}\),
  \begin{equation*}
    \coord{\vec{q}_1 + \vec{q}_2}{F} = \coord{\vec{q}_1}{F} + \coord{\vec{q}_2}{F}
  \end{equation*}
\end{itemize}

\subsection{Misc}

\begin{itemize}
\item If \(\uvec{u}\) is any unit vector and \(\rframe{F}\) is any frame, then
  \begin{equation*}
    \prn{\rdif{F}{\uvec{u}}} \vecdot \uvec{u} = 0
  \end{equation*}
\item If \(\uvec{u}\) and \(\uvec{v}\) are unit vectors,
  \(\uvec{u} \vecdot \uvec{v} = 0\) for all \(t\), and
  \(\rframe{F}\) is any frame, then
  \begin{equation*}
    \prn{\rdif{F}{\uvec{u}}} \vecdot \uvec{v} = -\uvec{u} \vecdot \prn{\rdif{F}{\uvec{v}}}
  \end{equation*}
\item For any vectors \(\vec{a} = a_x \ivec{F} + a_y \jvec{F} + a_z \kvec{F}\)
  and \(\vec{b} = b_x \ivec{F} + b_y \jvec{F} + b_z \kvec{F}\) and any frame
  \(\rframe{F}\),
  \begin{equation*}
    \coord{\vec{a} \vectimes \vec{b}}{F}
    = \begin{bmatrix}
      0 & -a_z & a_y \\
      a_z & 0 & -a_x \\
      -a_y & a_x & 0 \\
    \end{bmatrix}
    \begin{Bmatrix}
      b_x \\
      b_y \\
      b_z \\
    \end{Bmatrix}
    = \matcoord{\vec{a}\vectimes}{F} \coord{\vec{b}}{F}
  \end{equation*}
\item For any vectors \(\vec{a} = a_x \ivec{F} + a_y \jvec{F} + a_z \kvec{F}\),
  \(\vec{b} = b_x \ivec{F} + b_y \jvec{F} + b_z \kvec{F}\),
  \(\vec{c} = c_x \ivec{F} + c_y \jvec{F} + c_z \kvec{F}\) and any frame
  \(\rframe{F}\),
  \begin{align*}
    \vec{a} \vectimes (\vec{b} \vectimes \vec{c})
    &= \vec{b} (\vec{a} \vecdot \vec{c}) - \vec{c} (\vec{a} \vecdot \vec{b}) \\
    \vec{a} \vecdot (\vec{b} \vectimes \vec{c})
    &= \vec{b} \vecdot (\vec{c} \vectimes \vec{a})
      = \vec{c} \vecdot (\vec{a} \vectimes \vec{b})
  \end{align*}
\end{itemize}

\section{Kinetics}

\subsection{Second-order Tensors in 3D}

\begin{itemize}
\item The identity tensor, for any frame \(\rframe{F}\), is
  \begin{equation*}
    \tens{1} = \ivec{F}\ivec{F} + \jvec{F}\jvec{F} + \kvec{F}\kvec{F}
  \end{equation*}
  Note that for any vector \(\vec{c}\),
  \begin{equation*}
    \tens{1} \vecdot \vec{c} = \vec{c} \vecdot \tens{1} = \vec{c}
  \end{equation*}
\item Tensor expressed in \(\rframe{F} = \rframedef{F}\) frame
  \begin{equation*}
    \tens{T} =
    \prn{
      \begin{aligned}
        &\prn{\ivec{F} \vecdot \tens{T} \vecdot \ivec{F}} \ivec{F}\ivec{F}
        + \prn{\ivec{F} \vecdot \tens{T} \vecdot \jvec{F}} \ivec{F}\jvec{F}
        + \prn{\ivec{F} \vecdot \tens{T} \vecdot \kvec{F}} \ivec{F}\kvec{F} \\
        +\ &\prn{\jvec{F} \vecdot \tens{T} \vecdot \ivec{F}} \jvec{F}\ivec{F}
        + \prn{\jvec{F} \vecdot \tens{T} \vecdot \jvec{F}} \jvec{F}\jvec{F}
        + \prn{\jvec{F} \vecdot \tens{T} \vecdot \kvec{F}} \jvec{F}\kvec{F} \\
        +\ &\prn{\kvec{F} \vecdot \tens{T} \vecdot \ivec{F}} \kvec{F}\ivec{F}
        + \prn{\kvec{F} \vecdot \tens{T} \vecdot \jvec{F}} \kvec{F}\jvec{F}
        + \prn{\kvec{F} \vecdot \tens{T} \vecdot \kvec{F}} \kvec{F}\kvec{F} \\
      \end{aligned}
  }
  \end{equation*}
  or, in matrix form,
  \begin{equation*}
    \matcoord{\tens{T}}{F}
    = \begin{bmatrix}
      \ivec{F} \vecdot \tens{T} \vecdot \ivec{F} &
      \ivec{F} \vecdot \tens{T} \vecdot \jvec{F} &
      \ivec{F} \vecdot \tens{T} \vecdot \kvec{F} \\
      \jvec{F} \vecdot \tens{T} \vecdot \ivec{F} &
      \jvec{F} \vecdot \tens{T} \vecdot \jvec{F} &
      \jvec{F} \vecdot \tens{T} \vecdot \kvec{F} \\
      \kvec{F} \vecdot \tens{T} \vecdot \ivec{F} &
      \kvec{F} \vecdot \tens{T} \vecdot \jvec{F} &
      \kvec{F} \vecdot \tens{T} \vecdot \kvec{F} \\
    \end{bmatrix}
  \end{equation*}
\item For any frame \(\rframe{F}\), any tensor \(\tens{T}\), and any vector \(\vec{q}\),
  \begin{equation*}
    \coord{\tens{T} \vecdot \vec{q}}{F} = \matcoord{\tens{T}}{F} \coord{\vec{q}}{F}
  \end{equation*}
\item For any tensor \(\tens{T}\) and any reference frames \(\rframe{A}\) and \(\rframe{B}\),
  \begin{equation*}
    \matcoord{\tens{T}}{A} = \rot{A}{B} \matcoord{\tens{T}}{B} \rot{B}{A}
  \end{equation*}
\end{itemize}

\subsection{Moment of Inertia}

\begin{itemize}
\item Moment of inertia tensor of any system of point masses \(m_1,\ldots,m_N\)
  about any point \(P\) is
  \begin{equation*}
    \inertia{P} \equiv \sum_{i=1}^N \sbr{\prn{\pos{m_i/P} \vecdot \pos{m_i/P}} \tens{1} - \pos{m_i/P} \pos{m_i/P}} m_i
  \end{equation*}
\item Moment of inertia matrix of a rigid body consisting of point masses
  \(m_1,\ldots,m_N\) about point \(B\) in frame \(\rframe{B}\) is
  \begin{equation*}
    \matcoord{\inertia{B}}{B}
    = \begin{bmatrix}
      \inertiacomp{B}{xx/B} & -\inertiacomp{B}{xy/B} & -\inertiacomp{B}{xz/B} \\
      -\inertiacomp{B}{yx/B} & \inertiacomp{B}{yy/B} & -\inertiacomp{B}{yz/B} \\
      -\inertiacomp{B}{zx/B} & -\inertiacomp{B}{zy/B} & \inertiacomp{B}{zz/B} \\
    \end{bmatrix}
  \end{equation*}
  where
  \begin{align*}
    \inertiacomp{B}{xx/B} &= \sum_{i=1}^N m_i \prn{y_i^2 + z_i^2} &
    \inertiacomp{B}{xy/B} &= \sum_{i=1}^N m_i x_i y_i &
    \inertiacomp{B}{xz/B} &= \sum_{i=1}^N m_i x_i z_i \\
    \inertiacomp{B}{yx/B} &= \inertiacomp{B}{xy/B} &
    \inertiacomp{B}{yy/B} &= \sum_{i=1}^N m_i \prn{x_i^2 + z_i^2} &
    \inertiacomp{B}{yz/B} &= \sum_{i=1}^N m_i y_i z_i \\
    \inertiacomp{B}{zx/B} &= \inertiacomp{B}{xz/B} &
    \inertiacomp{B}{zy/B} &= \inertiacomp{B}{yz/B} &
    \inertiacomp{B}{zz/B} &= \sum_{i=1}^N m_i \prn{x_i^2 + y_i^2}
  \end{align*}
  where
  \begin{align*}
    x_i &= \pos{m_i/B} \vecdot \ivec{B} \\
    y_i &= \pos{m_i/B} \vecdot \jvec{B} \\
    z_i &= \pos{m_i/B} \vecdot \kvec{B}
  \end{align*}
\item Parallel axis theorem: For any point \(P\) and any system of particles
  with total mass \(m\) and center of mass \(\var{CM}\),
  \begin{equation*}
    \inertia{P} = \inertia{\var{CM}} + m \sbr{\prn{\pos{\var{CM/P}} \vecdot \pos{\var{CM/P}}} \tens{1} - \pos{\var{CM/P}} \pos{\var{CM/P}}}
  \end{equation*}
\end{itemize}

\subsection{Rigid Body}

\begin{itemize}
\item A system of particles \(m_1,\ldots,m_N\) constitutes a rigid body if
  there exists a frame \(\rframe{B}\) such that
  \begin{equation*}
    \rdif{B}{\pos{m_i/B}} = \vec{0} \quad \forall i = 1,\ldots,N \quad \forall t
  \end{equation*}
  \(\rframe{B}\) is called a ``body frame''.
\end{itemize}

\subsection{Angular Momentum}

\begin{itemize}
\item Angular momentum of point mass \(m\) with respect to point \(P\) as measured
  in \(\rframe{F}\) frame about point \(A\)
  \begin{equation*}
    \angmom{F}{A}{m/P} \equiv \pos{m/P} \vectimes m \vel{F}{m/A}
  \end{equation*}
\item Angular momentum of a system of \(N\) particles of mass \(m_i\) with respect to \(P\) as measured
  in \(\rframe{F}\) frame about point \(A\)
  \begin{equation*}
    \angmom{F}{A}{P,\text{sys}} \equiv \sum_i \pos{m_i/P} \vectimes m_i \vel{F}{m_i/A}
  \end{equation*}
\item For any inertial frame \(\rframe{O}\), any frame \(\rframe{F}\),
  any system of particles with total mass \(m\) and center of mass
  \(\var{CM}\), and any point \(P\),
  \begin{equation*}
    \angmom{O}{O}{P} = \angmom{F}{P}{P} + m \pos{\var{CM}/P} \vectimes \vel{O}{P/O} + \inertia{P} \vecdot \angvel{O}{F}
  \end{equation*}
\item For any inertial frame \(\rframe{O}\), any rigid body with total mass
  \(m\), center of mass \(\var{CM}\), and body frame
  \(\rframe{B} = \rframedef{B}\),
  \begin{equation*}
    \angmom{O}{O}{B} = m \pos{\var{CM}/B} \vectimes \vel{O}{B/O} + \inertia{B} \vecdot \angvel{O}{B}
  \end{equation*}
\item For any inertial frame \(\rframe{O}\), any rigid body with total mass
  \(m\) and center of mass \(\var{CM}\), and any point \(A\),
  \begin{equation*}
    \angmom{O}{O}{A} = \angmom{O}{O}{\var{CM}} + m \pos{\var{CM}/A} \vectimes \vel{O}{\var{CM}/O}
  \end{equation*}
\end{itemize}

\subsection{Newton's 2\textsuperscript{nd} Law and Generalizations}

\begin{itemize}
\item Newton's second law: for any inertial frame \(\rframe{O}\) with a system
  of \(N\) particles of mass \(m_i\) subjected to external forces \(\vec{F}\),
  \begin{equation*}
    \sum\vec{F} = m\accel{O}{\var{CM}/O} = m\rdif[2]{O}{\pos{\var{CM}/O}}
  \end{equation*}
  where the position \(\pos{\var{CM}/O}\) of the center of mass \(\var{CM}\) is given by
  \begin{equation*}
    \pos{\var{CM}/O} \equiv \frac{\sum_{i=1}^N m_i \pos{m_i/O}}{\sum_{i=1}^N m_i}
  \end{equation*}
  and the total mass \(m\) is given by
  \begin{equation*}
    m = \sum_{i=1}^N m_i
  \end{equation*}
\item Torque about point \(B\) created by external forces \(\vec{F}\) acting on point \(Q\)
  \begin{equation*}
    {\prn{\vec{\tau}_Q}}_B = \pos{Q/B} \vectimes \sum \vec{F}
  \end{equation*}
\item For any point \(P\), any inertial frame \(\rframe{O}\) with a system of
  particles, such that equal and opposite internal forces are collinear, i.e.\
  \emph{the internal torques cancel}, which has total mass \(m\) and center of
  mass \(\var{CM}\), and is subjected to external torques \(\vec{\tau}_B\),
  \begin{align*}
    \sum \vec{\tau}_{P,\text{sys}}
    &= \rdif{O}\prn{\angmom{O}{O}{P,\text{sys}}} + m \vel{O}{P/O} \vectimes \vel{O}{\var{CM}/O} \\
    &= \rdif{O}\prn{\angmom{O}{O}{P,\text{sys}}} + m \vel{O}{P/O} \vectimes \vel{O}{\var{CM}/P}
  \end{align*}
\item Euler's equations of motion: For any inertial frame \(\rframe{O}\), any
  rigid body with \emph{principal} body axes \(\rframe{B}\), total mass \(m\),
  and center of mass \(\var{CM}\),
  \begin{align*}
    \tau_{x\rframe{B}}
    &= \inertiacomp{B}{xx/\var{CM}} \dot{\omega}_{x\rframe{B}}
      - \prn{\inertiacomp{B}{yy/\var{CM}} - \inertiacomp{B}{zz/\var{CM}}} \omega_{y\rframe{B}} \omega_{z\rframe{B}} \\
    \tau_{y\rframe{B}}
    &= \inertiacomp{B}{yy/\var{CM}} \dot{\omega}_{y\rframe{B}}
      - \prn{\inertiacomp{B}{zz/\var{CM}} - \inertiacomp{B}{xx/\var{CM}}} \omega_{z\rframe{B}} \omega_{x\rframe{B}} \\
    \tau_{z\rframe{B}}
    &= \inertiacomp{B}{zz/\var{CM}} \dot{\omega}_{z\rframe{B}}
      - \prn{\inertiacomp{B}{xx/\var{CM}} - \inertiacomp{B}{yy/\var{CM}}} \omega_{x\rframe{B}} \omega_{y\rframe{B}}
  \end{align*}
  where
  \begin{align*}
    \angvel{O}{B} &= \omega_{x\rframe{B}}\ivec{B} + \omega_{y\rframe{B}}\jvec{B} + \omega_{z\rframe{B}}\kvec{B} \\
    \vec{\tau}_{\var{CM}} &= \tau_{x\rframe{B}}\ivec{B} + \tau_{y\rframe{B}}\jvec{B} + \tau_{z\rframe{B}}\kvec{B}
  \end{align*}
\end{itemize}

\subsection{Lagrange's Method}

\begin{itemize}
\item Kinetic energy of any system of particles with body frame \(\rframe{B}\),
  total mass \(m\), and center of mass \(\var{CM}\) is
  \begin{equation*}
    \mathcal{T} = \tfrac12 m \prn{\vel{O}{\var{CM}/O} \vecdot \vel{O}{\var{CM}/O}} + \tfrac12 \angvel{O}{B} \vecdot \inertia{\var{CM}} \vecdot \angvel{O}{B}
  \end{equation*}
\item For any system of particles subject to conservative forces, the Lagrangian is defined as
  \begin{equation*}
    \mathcal{L} = \mathcal{T} - \mathcal{V}
  \end{equation*}
  where
  \begin{align*}
    \mathcal{T} &= \text{sum of all kinetic energies of all particles} \\
    \mathcal{V} &= \text{total potential energy of the system}
  \end{align*}
\item For a conservative, holonomic system with generalized coordinates
  \(q_1,\ldots,q_N\) and Lagrangian \(\mathcal{L}\),
  \begin{align*}
    \dod{}{t}\prn{\dpd{\mathcal{L}}{\dot{q}_1}} - \dpd{\mathcal{L}}{q_1} &= 0 \\
    &\vdots \\
    \dod{}{t}\prn{\dpd{\mathcal{L}}{\dot{q}_N}} - \dpd{\mathcal{L}}{q_N} &= 0 \\
  \end{align*}
\end{itemize}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% coding: utf-8
%%% TeX-master: t
%%% TeX-engine: xetex
%%% End:
