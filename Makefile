SHELL = /bin/bash

.PHONY: all clean

all: dynamics.pdf

%.pdf: %.tex
	latexmk -pdf -pdflatex=xelatex "$<"

clean:
	git clean -fXdq
